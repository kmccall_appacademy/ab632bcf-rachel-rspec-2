
def reverser
  words = yield.split
  reversed = words.map do |el|
    el.reverse
  end
  reversed.join(" ")
end

def adder(def2 = 1)
  yield + def2
end

def repeater(def1 = 1)
  def1.times do
    yield
  end 
end
