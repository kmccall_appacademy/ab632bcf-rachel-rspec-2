
def measure(num_times=1)
  s = Time.now
  num_times.times { yield }
  e = Time.now
  (e - s) / num_times

end
